import { AppProps } from "next/app";
import Head from "next/head";

import "react-notion/src/styles.css";
import "prismjs/themes/prism-tomorrow.css";
import "bootstrap/dist/css/bootstrap.css";

import "../public/css/style.css";

export default function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <link rel="shortcut icon" href="/assets/favicon.ico" />
        <title>Chavyv Blog</title>
      </Head>
      <Component {...pageProps} />
    </>
  );
}
