import { NotionRenderer, BlockMapType } from "react-notion";
import Link from "next/link";

import { getAllPosts, Post } from "../";

export async function getStaticProps({
  params: { slug },
}: {
  params: { slug: string };
}) {
  // Get all posts again
  const posts = await getAllPosts();

  // Find the current blogpost by slug
  const post = posts.find((t) => t.slug === slug);

  const blocks = await fetch(
    `https://notion-api.splitbee.io/v1/page/${post!.id}`
  ).then((res) => res.json());

  return {
    props: {
      blocks,
      post,
    },
  };
}

const BlogPost: React.FC<{ post: Post; blocks: BlockMapType }> = ({
  post,
  blocks,
}) => {
  if (!post) return null;

  return (
    <div className="content justify-content-center">
      <h1 className="dark-brown mt-3 text-center">{post.page}</h1>
      <h5 className="light-brown text-center">Posted on {post.date}</h5>
      <div className="mt-5">
        <NotionRenderer blockMap={blocks} />
      </div>
      <Link href="/" as="/">
        <h4 className="mt-4 light-brown">Back</h4>
      </Link>
    </div>
  );
};

export async function getStaticPaths() {
  const table = await getAllPosts();
  return {
    paths: table.map((row) => `/blog/${row.slug}`),
    fallback: true,
  };
}

export default BlogPost;
