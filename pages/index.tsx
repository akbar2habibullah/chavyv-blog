import Link from "next/link";

const NOTION_BLOG_ID = process.env.NOTION_BLOG_ID;

export type Post = {
  id: string;
  slug: string;
  page: string;
  date: string;
  published: boolean;
};

export const getAllPosts = async (): Promise<Post[]> => {
  return await fetch(
    `https://notion-api.splitbee.io/v1/table/${NOTION_BLOG_ID}`
  ).then((res) => res.json());
};

export async function getStaticProps() {
  const posts = await getAllPosts();
  return {
    props: {
      posts: posts.filter((post) => post.published),
    },
  };
}

function HomePage({ posts }: { posts: Post[] }) {
  return (
    <div className="row">
      <header className="col-12 col-lg-6 p-5">
        <h1 className="brand">
          Chavyv<span className="dark-brown">.</span>Blog
        </h1>
        <img
          className="foto-profil rounded-circle my-3"
          src="/assets/fotoProfil.jpeg"
          alt="foto profil"
        />
        <p>
          Welcome to my blog! This is my place to share something about my life
          or random thoughts. Hope you enjoy this content!
        </p>
        <footer className="mt-5">
          Made with <a href="https://getbootstrap.com/">Bootstrap</a>,{" "}
          <a href="https://nextjs.org/">NextJS</a>, &{" "}
          <a href="https://www.notion.so">Notion</a>. Source available on{" "}
          <a href="https://gitlab.com/akbar2habibullah/chavyv-blog">Gitlab</a>{" "}
          <br />
          Copyright © { (new Date().getFullYear()) } <a href="https://www.chavyv.xyz/">Chavyv Akvar</a>.
          All Rights Reserved
        </footer>
      </header>
      <div className="col-12 col-lg-6 p-5">
        <h1>Posts</h1>
        <div className="posts">
          {posts.map((post) => (
            <Link href="/blog/[slug]" as={`/blog/${post.slug}`} key={post.slug}>
              <div className="card">
                <div className="card-body">
                  <div className="card-title">
                    <h2>{post.page}</h2>
                  </div>
                  <div className="card-subtitle">
                    <div>posted on {post.date}</div>
                  </div>
                </div>
              </div>
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
}

export default HomePage;
